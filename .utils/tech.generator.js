/*
 * Raman Marozau, 2018
 */

// branch: tech/interview-questions-generator

const fs = require('fs');

const { Logger, stop } = require('./utils');
const { createTopic, findTopicIndex } = require('./tech.utils');

const TECH_CONTENT_PATH = './1-technologies/1.1-content';
const TECH_DATA_DOC_FILENAME = '1.1.1-content.tree.md';
const TECH_DATA_PATH = `${TECH_CONTENT_PATH}/.apps/1.1-tech-content/src/.data`;
const TECH_DATA_FILENAME = '1.1.1-content.tree.json';

// @todo: root directory
// @cons: a really big problem
const isContentTreeDataDirectoryExist = fs.existsSync(TECH_DATA_PATH);

if (!isContentTreeDataDirectoryExist) {
  Logger.error('Interview data directory does not exist. Path: /1-technologies/1.1-content/.data');
  stop();
}

const topics = fs.readFileSync(`${TECH_CONTENT_PATH}/${TECH_DATA_DOC_FILENAME}`, 'utf-8');

const FIRST_LEVEL = 1;
const SECOND_LEVEL = 2;
const THIRD_LEVEL = 3;

const topicMarkdownList = topics.split('- ');
const prevTopicList = [];
const topicList = [];
const tree = [];

topicMarkdownList.forEach((item, i) => {
  if (i > 0) {
    const currentTheme = item.trim();
    const parent = topicMarkdownList[i - 1];
    const level = Math.floor((parent.length - parent.indexOf('\n'))/4);

    if (level === 0) {
      prevTopicList.splice(0, prevTopicList.length);
      prevTopicList.push(currentTheme);
    } else if (level > prevTopicList.length - 1) {
      prevTopicList.push(currentTheme);
    } else if (level < prevTopicList.length - 1) {
      prevTopicList.splice(prevTopicList.length - level - 1, prevTopicList.length);
      prevTopicList.push(currentTheme);
    } else if (level === prevTopicList.length - 1) {
      prevTopicList.pop();
      prevTopicList.push(currentTheme);
    }
    topicList.push([...prevTopicList]);
  }
});
topicList.forEach((topics) => {
  switch (topics.length) {
    case FIRST_LEVEL:
      tree.push(createTopic(topics[FIRST_LEVEL - 1]));
      break;
    case SECOND_LEVEL:
      tree[findTopicIndex(tree, topics[FIRST_LEVEL - 1])]
        .topics.push(createTopic(topics[SECOND_LEVEL - 1]));
      break;
    case THIRD_LEVEL:
      tree[findTopicIndex(tree, topics[FIRST_LEVEL - 1])]
        .topics[findTopicIndex(tree[findTopicIndex(tree, topics[FIRST_LEVEL - 1])].topics, topics[SECOND_LEVEL - 1])]
        .topics.push(createTopic(topics[THIRD_LEVEL - 1]));
      break;
    default:
      break;
  }
});

fs.writeFileSync(`${TECH_DATA_PATH}/${TECH_DATA_FILENAME}`, JSON.stringify(tree));
Logger.stack([['Technologies tree data is generated.\n']]);
