/*
 * Raman Marozau, 2019
 */

const { Logger, stop } = require('./utils');

const tree = [{
  title: "Testing::0",
  topics: [
    {
      title: "Levels::0::0",
      topics: [
        {
          title: "Unit Testing::0::0::0",
          topics: [{
            title: "Unit Testing Benefits::0::0::0::0",
            topics: [{
              title: "Confidence in changing/maintaining code::0::0::0::0::0",
              topics: [{
                title: "Maintaining::0::0::0::0::0::0",
                topics: [],
              }],
            }],
          }],
        }],
    }, {
      title: "Levels::0::1",
      topics: [
        {
          title: "Unit Testing::0::1::0",
          topics: [{
            title: "Unit Testing Benefits::0::1::0::0",
            topics: [{
              title: "Confidence in changing/maintaining code::0::1::0::0::0",
              topics: [{
                title: "Maintaining::0::1::0::0::0::0",
                topics: [],
              }],
            }, {
              title: "Confidence in changing/maintaining code::0::1::0::0::1",
              topics: [{
                title: "Maintaining::0::1::0::0::1::0",
                topics: [],
              }, {
                title: "Maintaining::0::1::0::0::1::1",
                topics: [],
              }, {
                title: "Maintaining::0::1::0::0::1::2", // this [0, 1, 0, 0, 1, 2]
                topics: [],
              }],
            }, {
              title: "Confidence in changing/maintaining code::0::1::0::0::2",
              topics: [{
                title: "Maintaining::0::1::0::0::2",
                topics: [],
              }],
            }],
          }],
        }],
    }, {
      title: "Levels::0::2",
      topics: [
        {
          title: "Unit Testing::0::2::0",
          topics: [{
            title: "Unit Testing Benefits::0::2::0::0",
            topics: [{
              title: "Confidence in changing/maintaining code::0::2::0::0::0",
              topics: [{
                title: "Maintaining::0::2::0::0::0::0",
                topics: [],
              }],
            }],
          }],
        }],
    }],
}, {
  title: "Testing::0",
  topics: [
    {
      title: "Levels::0::0",
      topics: [
        {
          title: "Unit Testing::0::0::0",
          topics: [{
            title: "Unit Testing Benefits::0::0::0::0",
            topics: [{
              title: "Confidence in changing/maintaining code::0::0::0::0::0",
              topics: [{
                title: "Maintaining::0::0::0::0::0::0",
                topics: [],
              }],
            }],
          }],
        }],
    }, {
      title: "Levels::0::1",
      topics: [
        {
          title: "Unit Testing::0::1::0",
          topics: [{
            title: "Unit Testing Benefits::0::1::0::0",
            topics: [{
              title: "Confidence in changing/maintaining code::0::1::0::0::0",
              topics: [{
                title: "Maintaining::0::1::0::0::0::0",
                topics: [],
              }],
            }, {
              title: "Confidence in changing/maintaining code::0::1::0::0::1",
              topics: [{
                title: "Maintaining::0::1::0::0::1::0",
                topics: [],
              }, {
                title: "Maintaining::0::1::0::0::1::1",
                topics: [],
              }, {
                title: "Maintaining::0::1::0::0::1::2", // this [0, 1, 0, 0, 1, 2]
                topics: [],
              }],
            }, {
              title: "Confidence in changing/maintaining code::0::1::0::0::2",
              topics: [{
                title: "Maintaining::0::1::0::0::2",
                topics: [],
              }],
            }],
          }],
        }],
    }, {
      title: "Levels::0::2",
      topics: [
        {
          title: "Unit Testing::0::2::0",
          topics: [{
            title: "Unit Testing Benefits::0::2::0::0",
            topics: [{
              title: "Confidence in changing/maintaining code::0::2::0::0::0",
              topics: [{
                title: "Maintaining::0::2::0::0::0::0",
                topics: [],
              }],
            }],
          }],
        }],
    }],
}, {
  title: "Testing::2",
  topics: [
    {
      title: "Levels::2::0",
      topics: [
        {
          title: "Unit Testing::2::0::0",
          topics: [{
            title: "Unit Testing Benefits::2::0::0::0",
            topics: [{
              title: "Confidence in changing/maintaining code::2::0::0::0::0",
              topics: [{
                title: "Maintaining::2::0::0::0::0::0",
                topics: [],
              }],
            }],
          }],
        }],
    }, {
      title: "Levels::2::1",
      topics: [
        {
          title: "Unit Testing::2::1::0",
          topics: [{
            title: "Unit Testing Benefits::2::1::0::0",
            topics: [{
              title: "Confidence in changing/maintaining code::2::1::0::0::0",
              topics: [{
                title: "Maintaining::2::1::0::0::0::0",
                topics: [],
              }],
            }, {
              title: "Confidence in changing/maintaining code::2::1::0::0::1",
              topics: [{
                title: "Maintaining::2::1::0::0::1::0",
                topics: [],
              }, {
                title: "Maintaining::2::1::0::0::1::1",
                topics: [],
              }, {
                title: "Maintaining::2::1::0::0::1::2", // this [0, 1, 0, 0, 1, 2]
                topics: [],
              }],
            }, {
              title: "Confidence in changing/maintaining code::2::1::0::0::2",
              topics: [{
                title: "Maintaining::2::1::0::0::2",
                topics: [],
              }],
            }],
          }],
        }],
    }, {
      title: "Levels::0::2",
      topics: [
        {
          title: "Unit Testing::0::2::0",
          topics: [{
            title: "Unit Testing Benefits::0::2::0::0",
            topics: [{
              title: "Confidence in changing/maintaining code::0::2::0::0::0",
              topics: [{
                title: "Maintaining::0::2::0::0::0::0",
                topics: [],
              }],
            }],
          }],
        }],
    }],
}];

// @cons:@todo: Create the simple function/functions/class/etc. to get/update/create/etc. some tree level

const IPPID = [2, 1, 0, 0, 1, 2];
const ippidString = '0.1.0.0.1.2';

let CACHE = [];

Logger.stack([[getByIppdid(tree, IPPID, tree)]]);

function setLevelTopics(tree, ippid, topics) {
  let tmp = [];
  let tmpTopics;

  ippid.forEach((level, index) => {
    tmp[level] = {
      title: tmp[level].title,
      topics: tmp[level].topics,
    };
    // tmp.title = tmp[level].title;
    // tmp.topics = tmp[level].topics;
    tmpTopics = tree[level].topics;
    if (index === ippid.length - 1) {

    }
  });
}

function getByIppdid(tree, ippid, lTree) {
  console.log('lTree: ', lTree);
  console.log('ippid: ', ippid);
  ippid.reduce((leafTree, level, index, _ippid) => {

    console.log('\nStart...');

    const currentIppid = _ippid.slice(0, index + 1);

    // Logger.stack([[currentIppid, level, index, _ippid, leafTree, '---']]);

    // console.log('leafTree: ', leafTree);
    // console.log('leafTree.topics: ', leafTree[level].topics);
    // console.log('leafTree[level].topics: ', leafTree[level].topics);
    if (index === level) {
      console.log('leafTree[level]: ', leafTree[level]);
      CACHE = CACHE.push({
        title: leafTree[level].title,
        topics: getByIppdid(leafTree[level].topics, ippid, lTree[level].topics.map((topic) => ({ ...topic }))),
      });
    } else {
      return leafTree;
      // return [{ title: CACHE[index].title, topics: CACHE[index].topics }];
    }

    CACHE = CACHE.push({
      title: leafTree[level].title,
      topics: getByIppdid(leafTree[level].topics, currentIppid, leafTree[level].topics),
    });

    console.log('CACHE: ', CACHE);

    console.log({ title: leafTree[0].title, topics: leafTree[0].topics });

    return { title: CACHE[index].title, topics: CACHE[index].topics };

    // const leafTreeElement = {
    //   title: tmpTree[level].title,
    //   topics: tmpTree[level].topics, // @tech: [leafTreeField]: tree[level][leafTreeField],
    // };
    // if (index === 0) {
    //   leafTree = [{ title: '', topics: [] }];
    // }
    // leafTree[/* the downward list of indexes*/].topics.push(leafTreeElement);
    //
    // console.log('leafTree: ', leafTree);
    // leafTree.push({
    //   title: tree[level].title,
    //   topics: tree[level].topics, // @tech: [leafTreeField]: tree[level][leafTreeField],
    // });
    // console.log('leafTree: ', leafTree);
    // add the tree item => the response is the simplest tree until looking for element
    // if (_ippdid.length !== ippid) {
      // Logger.stack([[level, index, getByIppdid(leafTree, _ippdid.slice(0, index + 1))]]);
    // }
    // if (_ippdid.slice(0, index + 1).length === _ippdid.length) {
    //   Logger.stack([[leafTree]]);
    //   return leafTree;
    // }
    /*
    Logger.stack([[level, index, _ippdid, leafTree, '---']]);
    // Logger.stack([[level, index, _ippdid, _ippdid.slice(0, index + 1), leafTree, '---']]);
    // console.log(getByIppdid(leafTree, _ippdid.slice(0, index + 1)));
    const currentIppid = _ippdid.slice(0, index + 1);
    console.log('\n');
    console.log(leafTree, currentIppid);
    console.log('\n');
    let tmpTree = Array.isArray(leafTree) ? leafTree : [leafTree]; // @cons: entry arguments
    console.log('tmpTree: ', tmpTree);
    currentIppid.forEach((level, index) => {
      tmpTree = tmpTree[level].topics;
    });
    // console.log('tmpTree: ', tmpTree);

    // for (index in currentIppid) {
    //
    // }
    const leafTreeElement = {
      title: tmpTree[level].title,
      topics: tmpTree[level].topics, // @tech: [leafTreeField]: tree[level][leafTreeField],
    };
    if (index === 0) {
      leafTree = [{ title: '', topics: [] }];
    }
    leafTree[the downward list of indexes].topics.push(leafTreeElement);

    console.log('leafTree: ', leafTree);


    // leafTree.push({
    //   title: tree[level].title,
    //   topics: tree[level].topics, // @tech: [leafTreeField]: tree[level][leafTreeField],
    // });

    console.log('leafTree: ', leafTree);

    return leafTree;
    */
  }, lTree);
}

Logger.stack([[]]);
