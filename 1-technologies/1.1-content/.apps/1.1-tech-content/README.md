# 1.1. Content. Technologies Helper.
Sense: interview, learning, etc. \
Keep the Technologies Tree text/list information actual over time.

### Information
Package commands are controlled from the project root.
- `npm run apps:tech:start`
    - Run the Technologies Helper application
- `npm run apps:tech:install`
    - Install dependencies of the Technologies Helper application
- `npm run apps:tech:gen:tree`
    - Generate the JSON Technologies Tree from [The Tree of Tech Unit Technologies](../../1.1.1-content.tree.md)
