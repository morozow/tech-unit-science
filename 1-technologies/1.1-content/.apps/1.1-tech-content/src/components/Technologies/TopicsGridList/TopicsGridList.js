/*
 * Raman Marozau, 2019
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { GridList } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import { TopicCard } from '../TopicGridItem';
import { findTopicIndex, isUndefined } from '../../../.utils';

const stylesList = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
  },
};

const styles = (theme) => ({
  root: {
    ...stylesList.root,
    backgroundColor: theme.palette.background.paper,
  },
  // @todo: conditional adding the style line, borderRight: '2px solid #ccc'
  rootFull: {
    ...stylesList.root,
    backgroundColor: theme.palette.background.paper,
    borderRight: '2px solid #065a9699',
  },
  rootFirst: {
    ...stylesList.root,
    backgroundColor: theme.palette.background.paper,
    boxShadow: '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
  },
  gridList: {
    flexWrap: 'nowrap',
    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    transform: 'translateZ(0)',
  },
  title: {
    color: theme.palette.primary.light,
  },
  titleBar: {
    background:
      'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
  },
});

class _TopicGridList extends Component {

  state = {
    tree: [], // The list of Topic of the current item
    topics: {
      actual: [], // selected the highest level topics
    },
  };

  get rootClassName() {
    return this.props.list.length > 1
      ? this.props.classes.rootFull
      : this.props.list.length === 1
        ? this.props.classes.rootFirst
        : this.props.classes.root;
  }

  constructor(props) {
    super(props);
    this.onTopicSelect = this.onTopicSelect.bind(this);
  }

  getActualTopicsList(mainTopicIndex) {
    return this.state.topics.actual
      .filter(({ topicIndex }) => topicIndex === mainTopicIndex)
      .map(({ topicIndex, index }) => this.props.list[mainTopicIndex].topics[index]);
  }

  componentDidMount() {
    this.state = {
      tree: this.props.list, // The list of Topics of the current item
      topics: {
        actual: [], // selected the highest level topics
      }
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.list !== prevProps.list) {
      this.setState((state, props) => ({
        ...state,
        tree: props.list,
      }))
    }
  }

  /**
   * @cons: the list of topics [mainTopicIndex, subMainTopicIndex, topicIndex, etc.]
   * -> the index of this list defines the level indeed
   * @param event
   * @param mainTopicIndex
   * @param index
   */
  onTopicSelect(event, mainTopicIndex, index) {
    this.setState((state, props) => {
      const isTopicExist = !isUndefined(state.topics.actual
        .find(({ topicIndex, index: i }) => topicIndex === mainTopicIndex && i === index));

      return {
        tree: state.tree.map((topic, topicIndex) => ({
          ...topic,
          isDone: topicIndex === mainTopicIndex && findTopicIndex(state.tree[mainTopicIndex].topics, topic.title) === index
            ? !topic.isDone
            : topic.isDone
        })),
        topics: {
          ...state.topics,
          actual: isTopicExist
            ? state.topics.actual
              .filter(({ topicIndex, index: i }) => topicIndex === mainTopicIndex && index !== i)
            : state.topics.actual.push({ topicIndex: mainTopicIndex, index }) && state.topics.actual,
        }
      }
    });
  }

  render() {
    return (
      <div className={this.rootClassName}>
        <GridList className={this.props.classes.gridList} cols={2.5}>
          {this.props.list && this.props.list.filter(Boolean).map((topic, topicIndex) =>
            <TopicCard
              onTopicClick={(event, index) => this.onTopicSelect(event, topicIndex, index)}
              topic={topic}
              list={this.getActualTopicsList(topicIndex)}
              key={topicIndex}/>
          )}
        </GridList>
      </div>
    );
  }
}

_TopicGridList.propTypes = {
  list: PropTypes.array,
};

export const TopicGridList = withStyles(styles)(_TopicGridList);
