/*
 * Raman Marozau, 2019
 */

import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles';

import { TopicCard } from './TopicGridItem';
import { TopicGridList } from './TopicsGridList';
import { findTopicIndex, createTopic, isUndefined } from '../../.utils';

import techTree from '../../.data/1.1.1-content.tree';

import './Technologies.css';

const THE_HIGHEST_TREE_LEVEL = 0; // @cons: the highest logic or informatics level: 0 | 1?

const styles = (theme) => ({
  root: {
    maxWidth: 400,
    minWidth: 400,
  },
});

export class _Technologies extends Component {

  get rootTopic() {
    // @cons: rootTopic has every level topic
    return {
      topic: 'Root',
      topics: this.state.tree,
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      tree: techTree, // TechTree
      topics: {
        actual: [], // selected the highest level topics
      }
    };
    this.onTopicSelect = this.onTopicSelect.bind(this);
  }

  getActualTopicsList() {
    return this.state.topics.actual.map((index) => this.state.tree[index]);
  }

  componentDidMount() {
    this.setState({
      tree: techTree,
    });

    // Logger.stack([[this.techTree]]); // @note: the common util, package, private
  }

  /**
   * Returns the list of the Topics of any level
   * @param {number} level
   * @return {Topic[]}
   */
  getLevelTree(level) {
    // 1: Topic[] -> topics :: 2: Topic[] -> topics :: 3: Topic[], ...
    return this.state.tree.map(({ title, topics }) => createTopic(title, topics));
  }

  onTopicSelect(event, index) {
    this.setState((state, props) => {
      const isTopicExist = !isUndefined(state.topics.actual.find((topicIndex) => topicIndex === index));

      return {
        tree: state.tree.map((topic, topicIndex) => ({
          ...topic,
          isDone: findTopicIndex(state.tree, topic.title) === index ? !topic.isDone : topic.isDone
        })),
        topics: {
          ...state.topics,
          actual: isTopicExist
            ? state.topics.actual.filter((topicIndex) => topicIndex !== index)/* && state.topics.actual*/
            : state.topics.actual.push(index) && state.topics.actual,
        }
      }
    });
  }

  render() {
    return (
      <div className={'Technologies'}>
        <div className={'Technologies__MainTopicsList'}>
          <TopicCard
            topic={this.rootTopic}
            title="Topics"
            subheader="The list of the highest level Topics"
            onTopicClick={(event, index) => this.onTopicSelect(event, index)}/>
        </div>

        <TopicGridList list={this.getActualTopicsList()}/>
      </div>
    );
  }
}

export const Technologies = withStyles(styles)(_Technologies);

