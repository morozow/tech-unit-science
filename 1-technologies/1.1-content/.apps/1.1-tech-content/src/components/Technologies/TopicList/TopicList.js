/*
 * Raman Marozau, 2019
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { IconButton, List, ListItem, ListItemSecondaryAction } from '@material-ui/core';
import { Done as DoneIcon, StarBorder as StartBorderIcon } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';

const styles = (theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing.unit * 4,
  },
  listItem: {
    height: 50,
  },
});

// @todo: replace the Main Section to component
// @todo: leave the TopicList as the second level and more implementation
export class _TopicList extends Component {

  // @cons: the similar interaction with styles
  static propTypes = {
    onTopicClick: PropTypes.func,
    list: PropTypes.array,
  };

  constructor(props) {
    super(props);
    this.state = {
      comment: '',
    };
  }

  renderStarIcon(isDone) {
    if (isDone) {
      return (
        <ListItemSecondaryAction>
          <IconButton aria-label="StartBorder">
            <StartBorderIcon />
          </IconButton>
        </ListItemSecondaryAction>
      );
    }
  }

  render() {
    return (
      <List
        component="nav"
        className={this.props.classes.root}>
        {this.props.list.map(({ title, topics, isDone }, index) =>
          <ListItem
            disabled={topics.length === 0}
            onClick={(event) => this.props.onTopicClick(event, index)} button key={index}
            className={this.props.classes.listItem}>
            {isDone ? <DoneIcon/> : void 0}
            {title}
            {this.renderStarIcon(isDone)}
          </ListItem>
        )}
      </List>
    );
  }
}

export const TopicList = withStyles(styles)(_TopicList);
