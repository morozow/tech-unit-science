/*
 * Raman Marozau, 2019
 */

import React, { Component } from 'react';

import { Card, CardHeader, CardContent } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import { TopicList } from '../TopicList';

const styles = (theme) => ({
  root: {
    maxWidth: 400,
    minWidth: 400,
    overflowY: 'auto',
    height: '100vh',
  },
  inner: {
    maxWidth: 400,
    minWidth: 400,
    overflowY: 'auto',
  },
});

class _TopicCard extends Component {

  render() {
    return (
      <Card className={this.props.inner ? this.props.classes.inner : this.props.classes.root }>
        <CardHeader
          title={this.props.title}
          subheader={this.props.subheader}>
        </CardHeader>
        <CardHeader title={this.props.topic.title}/>
        <CardContent>
          <TopicList
            list={this.props.topic.topics}
            onTopicClick={(event, topicIndex, index) => this.props.onTopicClick(event, topicIndex, index)}/>
          {this.props.list && this.props.list.filter(Boolean).map((topic, index) =>
            <TopicCard inner={true} topic={topic} key={index}/>
          )}
        </CardContent>
      </Card>
    );
  }
}

export const TopicCard = withStyles(styles)(_TopicCard);
