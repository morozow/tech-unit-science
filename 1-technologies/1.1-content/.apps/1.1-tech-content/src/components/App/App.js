/*
 * Raman Marozau, 2019
 */

import React, { Component } from 'react';

import { Technologies } from '../Technologies';

import './App.css';

export class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          {/*<h1>Technologies</h1>*/}
          <Technologies/>
        </header>
      </div>
    );
  }
}
