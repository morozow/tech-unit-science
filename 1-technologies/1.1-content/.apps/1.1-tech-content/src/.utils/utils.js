/*
 * Raman Marozau, 2019
 */

/**
 * Returns TRUE for undefined value
 * @param {Maybe<any>} value
 * @return {boolean}
 */
function isUndefined(value) {
  return value === undefined;
}

module.exports = {
  isUndefined,
};
