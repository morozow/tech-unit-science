/*
 * Raman Marozau, 2019
 */

const { Logger } = require('../../../../../../.utils/utils');

const tree = [{
  title: "Testing::0",
  topics: [
    {
      title: "Levels::0::0",
      topics: [
        {
          title: "Unit Testing::0::0::0",
          topics: [{
            title: "Unit Testing Benefits::0::0::0::0",
            topics: [{
              title: "Confidence in changing/maintaining code::0::0::0::0::0",
              topics: [{
                title: "Maintaining::0::0::0::0::0::0",
                topics: [],
              }],
            }],
          }],
        }],
    }, {
      title: "Levels::0::1",
      topics: [
        {
          title: "Unit Testing::0::1::0",
          topics: [{
            title: "Unit Testing Benefits::0::1::0::0",
            topics: [{
              title: "Confidence in changing/maintaining code::0::1::0::0::0",
              topics: [{
                title: "Maintaining::0::1::0::0::0::0",
                topics: [],
              }],
            }, {
              title: "Confidence in changing/maintaining code::0::1::0::0::1",
              topics: [{
                title: "Maintaining::0::1::0::0::1::0",
                topics: [],
              }, {
                title: "Maintaining::0::1::0::0::1::1",
                topics: [],
              }, {
                title: "Maintaining::0::1::0::0::1::2", // this [0, 1, 0, 0, 1, 2]
                topics: [],
              }],
            }, {
              title: "Confidence in changing/maintaining code::0::1::0::0::2",
              topics: [{
                title: "Maintaining::0::1::0::0::2",
                topics: [],
              }],
            }],
          }],
        }],
    }, {
      title: "Levels::0::2",
      topics: [
        {
          title: "Unit Testing::0::2::0",
          topics: [{
            title: "Unit Testing Benefits::0::2::0::0",
            topics: [{
              title: "Confidence in changing/maintaining code::0::2::0::0::0",
              topics: [{
                title: "Maintaining::0::2::0::0::0::0",
                topics: [],
              }],
            }],
          }],
        }],
    }],
}];

const IPPID = [0, 1, 0, 0, 1, 2];
const ippidString = '0.1.0.0.1.2';

Logger.stack([[getByIppdid(IPPID)]]);

function getByIppdid(tree, ippid, leafTreeField) {
  ippid.reduce((leafTree, level, index) => {
    // add the tree item => the response is the simplest tree until looking for element
    leafTree.push({
      title: tree[level].title,
      topics: tree[level].topics,
    });

    return leafTree;
  }, []);
}

Logger.stack([[]]);
