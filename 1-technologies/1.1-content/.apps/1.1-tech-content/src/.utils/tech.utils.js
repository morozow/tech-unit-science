/*
 * Raman Marozau, 2019
 */

/**
 * Creates the Topic according to the Topic Structure: title, subtopics.
 * Subtopics are considered Topic Structure. Downward Tree.
 * @param {string} title
 * @param {Topic[]} topics
 * @returns {{title: string, topics: Topic[]}}
 */
function createTopic(title, topics = []) {
  return { title, topics };
}

/**
 * Returns the current level Topic index in the Tree
 * @param {[]} arr
 * @param {string} title
 * @returns {number}
 */
function findTopicIndex(arr, title) {
  let index = 0;
  arr.forEach((topic, i) => {
    if (title === topic.title) {
      index = i;
    }
  });
  return index;
}

module.exports = {
  createTopic,
  findTopicIndex,
};
