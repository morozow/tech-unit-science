# 1. Technologies

### Content
- 1.1. [Javascript Technologies information](1.1-content/README.md)
    - 1.1.1. [The Tree of Tech Unit Technologies](1.1-content/1.1.1-content.tree.md)
    - 1.1.2. [JS News](1.1-content/1.1.2-content.news.md)
    - 1.1.3. [Garbage Can](1.1-content/1.1.3-content.garbage-can.md)
    - [Apps](1.1-content/.apps/README.md)
        - [Technologies Helper](1.1-content/.apps/1.1-tech-content/README.md)
- 1.2. [Frameworks, libraries and technologies for projects](1.2-project/README.md)
    - 1.2.1. [Arguments for React](1.2-project/1.2.1-project.react.md)
    - 1.2.2. [Arguments for Angular](1.2-project/1.2.2-project.angular.md)
    - 1.2.3 [Package Builder](1.2-project/1.2.3-project.builder.md)
- 1.3. [Interview](1.3-interview/README.md)
    - 1.3.1. [Angular Interview Questions](1.3-interview/1.3.1-interview.angular.md)
- 1.4. [Notes](1.4-notes/README.md)
    - 1.4.1. [Electron](1.4-notes/1.4.1-notes.electron.md)
    - 1.4.2. [Mobile Frameworks](1.4-notes/1.4.2-notes.mobile-frameworks.md)
    - 1.4.3. [Egghead information](1.4-notes/1.4.3-notes.egghead.md)
    - 1.4.4. [AWS Lambda Temporary Notes](1.4-notes/1.4.4-notes.aws-lambda.md)
    