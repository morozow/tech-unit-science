# 1.4. Notes

### 1.4. Content
- 1.4.1. [Electron](1.4.1-notes.electron.md)
- 1.4.2. [Mobile Frameworks](1.4.2-notes.mobile-frameworks.md)
- 1.4.3. [Egghead information](1.4.3-notes.egghead.md)
- 1.4.4. [AWS Lambda Temporary Notes](1.4.4-notes.aws-lambda.md)