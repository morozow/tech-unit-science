# 1.2. Frameworks, libraries and technologies for projects

### Content
- 1.2.1 [Arguments for React](1.2.1-project.react.md)
- 1.2.2 [Arguments for Angular](1.2.2-project.angular.md)
- 1.2.3 [Package Builder](1.2.3-project.builder.md)